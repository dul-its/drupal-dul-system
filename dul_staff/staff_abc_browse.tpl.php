<?php

/**
 * @file
 * Default theme implementation to display a grid of staff members with last name
 * beginning with a given letter.
 *
 * Available variables:
 * - $staff_members: array of stdClass Objects; all viewable staff for this view, A-Z
 * - $abc_nav: array of strings: each letter that should be clickable in A-Z nav
 */
?>

<!-- uncomment print to inspect records in browser -->
<?php #print kpr($staff_members, TRUE, "Staff Members"); ?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<?php # Pull in A-Z letter browse partial template ?>
<?php include(drupal_get_path('module', 'dul_staff').'/abc_names_nav.tpl.php'); ?>

<div id="directory-main-content" class="container abc-view" itemscope itemtype="http://schema.org/Organization">
  <meta itemprop="name" content="Duke University Libraries" />
  <meta itemprop="url" content="https://library.duke.edu" />

  <div class="row">

    <div class="col-sm-12">

      <div class="people-grid">
        <?php $people = $staff_members ?>
        <?php include_once 'people_grid.tpl.php'; ?>
      </div>

      <hr/>

    </div>

  </div>
</div>
