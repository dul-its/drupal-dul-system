<?php

/**
 * @file
 * Partial reusable template to render people in a grid
 */
?>


<?php $first_letter = 'a'; ?>

<div class="row people-row" id="names_a">

  <div class="people-grid">

  <?php foreach($people as $person):

    $current_first_letter = strtolower(substr($person->last_name, 0, 1));

    if ($current_first_letter != $first_letter) {

      $first_letter = $current_first_letter;

      print '</div>';

      print '</div>';

      print '<div class="row people-row" id="names_' . $first_letter . '">';

      print '<div class="people-grid">';

    }
    
  ?>

    <div class="person-wrap">
      
        <?php dul_staff_person_photo($person, 'staff_dir_thumb'); ?>

      <div class="person-name">
          <?php print $person->display_name; ?>
      </div>

      <div class="person-job-title">
        <?php print $person->preferred_title ? $person->preferred_title : $person->title; ?>
      </div>

      <ul class="contact-info list-unstyled">
        <?php if (!empty($person->email) ): //&& $person->email_privacy == '0' ?>
          <li class="contact-email" ititle="<?php print $person->email ?>"><?php print $person->email ?></li>
        <?php endif ?>
        <?php if (!empty($person->phone)): ?>
          <li class="contact-phone" >
            <?php print $person->phone ?>
          </li>
        <?php endif ?>
      </ul>

    </div>

  <?php endforeach; ?>

  </div>

</div>
