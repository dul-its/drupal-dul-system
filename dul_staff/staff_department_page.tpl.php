<?php

/**
 * @file
 * Default theme implementation to display DUL department page
 *
 * Available variables:
 * - $dept: stdClass Object for current department matched by id or slug
 * - $dept_tree: array of nested stdClass Objects; all departments reflecting hierarchy
 * - $dept_members: array of stdClass Objects; people in this dept or any of its
 *                  descendant depts
 */
?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<!-- uncomment print to inspect records in browser -->
<?php #print kpr($dept, TRUE, "Current Department"); ?>
<?php #print kpr($dept_tree, TRUE, "Dept Tree"); ?>
<?php #print kpr($dept_members, TRUE, "Dept Members"); ?>

<div id="directory-main-content" class="container dept-view" itemscope itemtype="http://schema.org/Organization">

  <div class="row">

    <div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">
      <?php # Render the page title here in the main column. The default title at the ?>
      <?php # top of page is hidden via CSS, which we may want to revisit in future. ?>
      <div class="page-header">
        <h1 class="page-title" id="dept-name" itemprop="name"><?php print $dept->name ?></h1>
      </div>


      <?php $has_description_or_url = ( $dept->description or $dept->url ? true : false ) ?>
      <?php $has_any_contact_info = ( $dept->phone or $dept->email or $dept->campus_box or $dept->physical_address ? true : false ) ?>

      <?php # Only render the dept info bar if there's something to put in it. ?>
      <?php if($has_description_or_url or $has_any_contact_info): ?>
        <div class="dept-info">
          <div class="row">
            <?php if($has_description_or_url): ?>
              <div class="dept-about-column col-sm-12 <?php if(!$has_any_contact_info) { print 'col-md-12'; } else if($dept->description ? print "col-md-8" : print "col-md-4") ?>">
                <?php if (!empty($dept->description)): ?>

                  <section id="dept-description-wrapper" class="expandable-content-wrapper">
                    <div class="expandable-content dept-description" itemprop="description">
                      <?php print $dept->description; ?>
                    </div>

                    <div class="expandable-content-controls">
                      <span class='show-control more'><a href="javascript:void(0);" class="btn btn-sm btn-show">show more <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a></span>
                      <span class='show-control less'><a href="#dept-name" class="btn btn-sm btn-hide">show less <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a></span>
                    </div>
                  </section>

                <?php endif; ?>
                <?php if (!empty($dept->url)): ?>
                  <a class="btn btn-primary btn-sm" href="<?php print $dept->url; ?>">View website &raquo;</a><br/>
                <?php endif; ?>
              </div>
            <?php endif; ?>
            <?php if($has_any_contact_info): ?>
              <div class="dept-contact-info-column col-sm-12 <?php if($dept->description ? print "col-md-4 one-column" : print "col-md-8") ?>">
                <ul class="contact-info list-unstyled">
                  <?php if (!empty($dept->phone)): ?>
                    <li class="contact-phone" itemprop="telephone">
                      <?php print $dept->phone ?>
                    </li>
                  <?php endif ?>
                  <?php if (!empty($dept->phone2)): ?>
                    <li class="contact-phone" itemprop="telephone"><?php print $dept->phone2 ?></li>
                  <?php endif ?>
                  <?php if (!empty($dept->fax)): ?>
                    <li class="contact-fax" itemprop="faxNumber"><?php print $dept->fax ?> <small class="text-muted">(fax)</small></li>
                  <?php endif ?>
                  <?php if (!empty($dept->email)): ?>
                    <li class="contact-email" itemprop="email"><a href="mailto:<?php print $dept->email ?>"><?php print $dept->email ?></a></li>
                  <?php endif ?>
                  <?php if (!empty($dept->campus_box)): ?>
                    <li class="contact-box" itemprop="address">Campus Box <?php print $dept->campus_box ?></li>
                  <?php endif ?>
                  <?php if (!empty($dept->physical_address)): ?>
                    <li class="contact-address" itemprop="address">
                      <?php print $dept->physical_address ?>
                    </li>
                  <?php endif; ?>
                </ul>
                <?php if (!empty($dept->contact_form_url)): ?>
                    <a href="<?php print $dept->contact_form_url ?>" class="btn btn-success btn-xs contact-url">
                      <i class="fa fa-comment" aria-hidden="true"></i> <?php if($dept->contact_form_url_label_text ? print $dept->contact_form_url_label_text : print "Contact Us") ?>
                    </a>
                  <?php endif ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif ?>

      <div class="people-grid">
        <?php $people = $dept_members ?>
        <?php include_once 'people_grid.tpl.php'; ?>
      </div>

      <hr/>

    </div>

    <nav class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9" role="navigation" aria-label="Navigate all library divisions and departments">
      <?php # Pull in sidebar as partial template for reuse on EG page ?>
      <?php include(drupal_get_path('module', 'dul_staff').'/dept_sidebar_menu.tpl.php'); ?>
    </nav>
  </div>
</div>
