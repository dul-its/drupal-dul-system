<?php

/**
 * @file
 * Partial reusable template to render a hierarchical department nav in a sidebar
 */
?>

<div class="panel-group dept-nav">

  <div class="panel panel-default dept-1">
    <div class="panel-heading<?php echo ($dept->id === 0 ? ' active' : '' ) ?>">
      <div class="panel-title">
        <a href="<?php print $GLOBALS['executive_group_path'] ?>">Library Executive Group</a>
      </div>
    </div>
  </div>

  <?php foreach($dept_tree as $d1): ?>
    <?php // Check if current dept is active/visible in the menu ?>
    <?php $is_active = ($d1->id == $dept->id ? true : false ) ?>
    <?php $has_active_descendant = (in_array($d1->id, $dept->ancestor_ids) ? true : false ) ?>

    <div class="panel panel-default dept-1">
      <div class="panel-heading<?php echo (($is_active or $has_active_descendant) ? ' active' : '' ) ?>" id="<?php print $d1->slug; ?>">
        <div class="panel-title">
          <?php if($d1->children): ?>
            <a class="collapse-control<?php echo (($is_active or $has_active_descendant) ? '' : ' collapsed' ) ?>" role="button" data-toggle="collapse" href="#collapse-<?php print $d1->slug; ?>" aria-controls="collapse-<?php print $d1->slug; ?>" aria-label="toggle subdepartments in <?php print $d1->name; ?>"></a>
          <?php endif; ?>

          <a href="<?php print $GLOBALS['dept_path'] . $d1->slug; ?>"><?php print $d1->name; ?></a>

        </div>
      </div>
      <div id="collapse-<?php print $d1->slug; ?>" class="dept-2 panel-collapse collapse<?php echo (($is_active or $has_active_descendant) ? ' in' : '' ) ?>" aria-labelledby="<?php print $d1->slug; ?>">
        <ul class="list-group">
          <?php foreach($d1->children as $d2): ?>
            <?php $is_active = ($d2->id == $dept->id ? true : false ) ?>
            <?php $has_active_descendant = (in_array($d2->id, $dept->ancestor_ids) ? true : false ) ?>

            <li class="list-group-item<?php echo (($is_active or $has_active_descendant) ? ' active' : '' ) ?>">
              <?php if($d2->children): ?>
                <a class="collapse-control<?php echo (($is_active or $has_active_descendant) ? '' : ' collapsed' ) ?>" role="button" data-toggle="collapse" href="#collapse-<?php print $d2->slug; ?>" aria-controls="collapse-<?php print $d2->slug; ?>" aria-label="toggle subdepartments in <?php print $d2->name; ?>"></a>
              <?php endif; ?>

              <a href="<?php print $GLOBALS['dept_path'] . $d2->slug; ?>"><?php print $d2->name; ?></a><br/>
              <?php if($d2->children): ?>
                <ul id="collapse-<?php print $d2->slug; ?>" class="dept-3 collapse<?php echo (($is_active or $has_active_descendant) ? ' in' : '' ) ?>" aria-labelledby="<?php print $d2->slug; ?>">
                  <?php foreach($d2->children as $d3): ?>
                    <?php $is_active = ($d3->id == $dept->id ? true : false ) ?>
                    <?php $has_active_descendant = (in_array($d3->id, $dept->ancestor_ids) ? true : false ) ?>

                    <li>
                      <a class="<?php echo (($is_active or $has_active_descendant) ? 'active' : '' ) ?>" href="<?php print $GLOBALS['dept_path'] . $d3->slug; ?>"><?php print $d3->name; ?></a>
                      <?php if($d3->children): ?>
                        <ul class="dept-4">
                          <?php foreach($d3->children as $d4): ?>
                            <?php $is_active = ($d4->id == $dept->id ? true : false ) ?>
                            <li>
                              <a class="<?php echo ($is_active ? 'active' : '' ) ?>" href="<?php print $GLOBALS['dept_path'] . $d4->slug; ?>"><?php print $d4->name; ?></a>
                            </li>
                          <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>

            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>

  <?php endforeach; ?>

  <!-- insert link for orgchart -->
  <div class="panel panel-default dept-1">
    <div class="panel-heading" id="orgchart">
      <div class="panel-title">
        
        <a href="/about/orgchart"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Organizational Chart <span class="sr-only">(PDF)</span></a>

      </div>
    </div>
  </div>

</div>
