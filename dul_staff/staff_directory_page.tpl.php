<?php

/**
 * @file
 * Default theme implementation to display DUL Staff Directory page
 *
 * Available variables:
 * - $staff_members: An array of all staff members
 * - $abc_nav: An array of letters with matching last names
 */
?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>


  <?php # Pull in A-Z letter browse partial template ?>
  <?php include(drupal_get_path('module', 'dul_staff').'/abc_names_nav.tpl.php'); ?>

  <div id="directory-main-content" class="people-grid">

    <?php $people = array_slice($staff_members,0,23) ?>
    <?php include_once 'people_grid.tpl.php'; ?>

    <div class="person-wrap">
      <a class="browse-more-people" href="<?php print $GLOBALS['browse_path']; ?>all">
        All Staff A-Z &raquo;
      </a>
    </div>
  </div>
