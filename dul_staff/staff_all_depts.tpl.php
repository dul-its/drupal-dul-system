<?php

/**
 * @file
 * Default theme implementation to display a page listing all depts
 *
 * Available variables:
 * - $dept_tree: array of nested stdClass Objects; all departments reflecting hierarchy
 * - $dept_list: array of flat stdClass Objects; all departments in alpha order
 */
?>
<!-- uncomment print to inspect records in browser -->
<?php #print kpr($dept_tree, TRUE, "Department Tree"); ?>
<?php #print kpr($dept_list, TRUE, "Department List"); ?>

<?php
  drupal_add_js('sites/all/modules/dul_system/dul_staff/js/dul_staff_dept_filter.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<div class="container">


  <div class="row">
    <div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3 all-depts">

        <form id="dept-filter" class="directory-filter">
          <label for="dept-search">Filter by Department Name</label>
          <div class="search-wrapper">
            <input type="text" id="dept-search" name="dept-search"/>
          </div>
        </form>


      <div id="directory-main-content" itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="Duke University Libraries" />
        <meta itemprop="url" content="https://library.duke.edu" />

        <?php # ==== TOP-LEVEL DEPARTMENTS ===== ?>
        <?php foreach($dept_tree as $d1): ?>
          <div class="panel panel-default dept-group dept-1">
            <div class="panel-heading" itemprop="department" itemscope itemtype="https://schema.org/Organization">

              <?php if (!empty($d1->phone)): ?>
                  <ul class="contact-info list-unstyled list-unstyled">
                    <?php if (!empty($d1->phone)): ?>
                      <li class="contact-phone">
                        <?php print $d1->phone ?>
                      </li>
                    <?php endif ?>
                  </ul>
              <?php endif; ?>

              <a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d1->slug; ?>" itemprop="url">
                <span itemprop="name"><?php print $d1->name; ?></span>
              </a>

              <?php if(isset($d1->dept_head->id)): ?>
                <div class="dept-head">
                  <div class="dept-head-name"><?php print $d1->dept_head->display_name ?></div>
                  <div class="dept-head-title"><?php print $d1->dept_head->preferred_title ? $d1->dept_head->preferred_title : $d1->dept_head->title; ?></div>
                </div>
              <?php endif ?>

              <?php if(isset($d1->dept_head_asst->id)): ?>
                <div class="dept-head">
                  <div class="dept-head-name"><?php print $d1->dept_head_asst->display_name ?></div>
                  <div class="dept-head-title"><?php print $d1->dept_head_asst->preferred_title ? $d1->dept_head_asst->preferred_title : $d1->dept_head_asst->title; ?></div>
                </div>
              <?php endif ?>

            </div>

            <?php if($d1->children): ?>
              <div class="panel-body">

                <?php # ==== SECOND-LEVEL DEPARTMENTS ===== ?>
                <?php foreach($d1->children as $d2): ?>

                  <div class="dept-2" itemprop="department" itemscope itemtype="https://schema.org/Organization">
                    <?php if (!empty($d2->phone)): ?>
                      <ul class="contact-info list-unstyled list-inline">
                        <?php if (!empty($d2->phone)): ?>
                          <li class="contact-phone">
                            <?php print $d2->phone ?>
                          </li>
                        <?php endif ?>
                      </ul>
                    <?php endif ?>

                    <a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d2->slug; ?>" itemprop="url">
                      <span itemprop="name"><?php print $d2->name; ?></span>
                    </a>

                    <?php if(isset($d2->dept_head->id)): ?>
                      <div class="dept-head">
                        <span class="dept-head-name"><?php print $d2->dept_head->display_name ?></span> &mdash; 
                        <span class="dept-head-title"><?php print $d2->dept_head->preferred_title ? $d2->dept_head->preferred_title : $d2->dept_head->title; ?></span>
                      </div>
                    <?php endif ?>

                    <?php if(isset($d2->dept_head_asst->id)): ?>
                      <div class="dept-head">
                        <span class="dept-head-name"><?php print $d2->dept_head_asst->display_name ?></span> &mdash; 
                        <span class="dept-head-title"><?php print $d2->dept_head_asst->preferred_title ? $d2->dept_head_asst->preferred_title : $d2->dept_head_asst->title; ?></span>
                      </div>
                    <?php endif ?>
                    
                    <?php if($d2->children): ?>
                      <div class="dept-2-children">

                        <?php # ==== THIRD-LEVEL DEPARTMENTS ===== ?>
                        <?php foreach($d2->children as $d3): ?>
                          <div class="dept-3" itemprop="department" itemscope itemtype="https://schema.org/Organization">

                            <?php if (!empty($d3->phone)): ?>
                              <ul class="contact-info list-unstyled list-inline">
                                <?php if (!empty($d3->phone)): ?>
                                  <li class="contact-phone">
                                    <?php print $d3->phone ?>
                                  </li>
                                <?php endif ?>
                              </ul>
                            <?php endif ?>

                            <a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d3->slug; ?>" itemprop="url">
                              <span itemprop="name"><?php print $d3->name; ?></span>
                            </a>
                            <?php if(isset($d3->dept_head->id)): ?>
                              <div class="dept-head">
                                <span class="dept-head-name"><?php print $d3->dept_head->display_name ?></span> &mdash; 
                                <span class="dept-head-title"><?php print $d3->dept_head->preferred_title ? $d3->dept_head->preferred_title : $d3->dept_head->title; ?></span>
                              </div>
                            <?php endif ?>

                            <?php if(isset($d3->dept_head_asst->id)): ?>
                              <div class="dept-head">
                                <span class="dept-head-name"><?php print $d3->dept_head_asst->display_name ?></span> &mdash; 
                                <span class="dept-head-title"><?php print $d3->dept_head_asst->preferred_title ? $d3->dept_head_asst->preferred_title : $d3->dept_head_asst->title; ?></span>
                              </div>
                            <?php endif ?>


                            <?php # ==== FOURTH-LEVEL DEPARTMENTS ===== ?>
                            <?php foreach($d3->children as $d4): ?>
                              <div class="dept-4" itemprop="department" itemscope itemtype="https://schema.org/Organization">

                                <?php if (!empty($d4->phone)): ?>
                                  <ul class="contact-info list-unstyled list-inline">
                                    <?php if (!empty($d4->phone)): ?>
                                      <li class="contact-phone">
                                        <?php print $d4->phone ?>
                                      </li>
                                    <?php endif ?>
                                  </ul>
                                <?php endif ?>

                                <a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d4->slug; ?>" itemprop="url">
                                  <span itemprop="name"><?php print $d4->name; ?></span>
                                </a>
                                <?php if(isset($d4->dept_head->id)): ?>
                                  <div class="dept-head">
                                    <span class="dept-head-name"><?php print $d4->dept_head->display_name ?></span> &mdash; 
                                    <span class="dept-head-title"><?php print $d4->dept_head->preferred_title ? $d4->dept_head->preferred_title : $d4->dept_head->title; ?></span>
                                  </div>
                                <?php endif ?>

                                <?php if(isset($d4->dept_head_asst->id)): ?>
                                  <div class="dept-head">
                                    <span class="dept-head-name"><?php print $d4->dept_head_asst->display_name ?></span> &mdash; 
                                    <span class="dept-head-title"><?php print $d4->dept_head_asst->preferred_title ? $d4->dept_head_asst->preferred_title : $d4->dept_head_asst->title; ?></span>
                                  </div>
                                <?php endif ?>

                              </div>
                            <?php endforeach; ?>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    <?php endif ?>
                  </div>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>


      <div id="dept-filter-results">
        <h2>Departments Matching Your Search</h2>
        <ul>
          <?php foreach($dept_list as $d): ?>
            <li>
              <a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d->slug; ?>">
                <?php print $d->name; ?>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>

    <nav class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9" role="navigation" aria-label="Navigate all library divisions and departments">
      <?php # Pull in sidebar as partial template for reuse on EG page ?>
      <?php include(drupal_get_path('module', 'dul_staff').'/dept_sidebar_menu.tpl.php'); ?>
    </nav>


</div>
