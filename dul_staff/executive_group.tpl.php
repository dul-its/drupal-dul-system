<?php

/**
 * @file
 * Default theme implementation to display DUL department page
 *
 * Available variables:
 * - $dept_tree: array of nested stdClass Objects; all departments reflecting hierarchy
 * - $head_executive: stdClass Object: head person of dept w/id = 1 (Perkins System)
 * - $eg_members: array of stdClass Objects: all members of the EG, head executive first
 */
?>

<!-- uncomment print to inspect records in browser -->
<?php #print kpr($eg_members, TRUE, "Executive Group Members"); ?>
<?php #print kpr($head_executive, TRUE, "Head Executive"); ?>
<?php #print kpr($dept_tree, TRUE, "Dept Tree"); ?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<div id="directory-main-content" class="container eg-page">

  <div class="row">

    <div class="col-sm-9 col-sm-push-3">
      <?php # Render the page title here in the main column. The default title at the ?>
      <?php # top of page is hidden via CSS, which we may want to revisit in future. ?>
      <div class="page-header">
        <h1 class="page-title">Library Executive Group</h1>
      </div>

      <?php foreach($eg_members as $member): ?>

      <?php // fallback to ID if slug is not present
        if ($member->slug) {
          $slugLink = $member->slug;
        } else {
          $slugLink = $member->id;
        }
      ?>

        <!-- Left-aligned -->
        <div class="media eg-member">
          <div class="media-left">
            <img class="person-thm media-object"
              src="<?php print $member->photo_url ?: $GLOBALS['devil_missing_photo_path']; ?>"
              alt="<?php print $member->display_name; ?>" style="min-width: 150px;"/><br/>
          </div>
          <div class="media-body">
            <h2 class="person-name">
              <a href="<?php print $GLOBALS['staff_profile_path'] . $slugLink; ?>"><?php print $member->display_name; ?></a>
            </h2>
            <div class="person-job-title">
              <?php print $member->preferred_title ? $member->preferred_title : $member->title; ?>
            </div>

            <ul class="contact-info list-unstyled">
              <?php if (!empty($member->phone)): ?>
                <li class="contact-phone">
                  <?php print $member->phone ?>
                </li>
              <?php endif ?>
              <?php if (!empty($member->email)): ?>
                <li class="contact-email"><a href="mailto:<?php print $member->email ?>"><?php print $member->email ?></a></li>
              <?php endif ?>
              <?php # TODO: Data in Duke HR is not clean; do we need the full box & physical addresses? ?>
              <?php if (!empty($member->campus_box[0])): ?>
                <li class="contact-box">
                  <?php foreach($member->campus_box as $addr_line): ?>
                    <?php print $addr_line ?><br/>
                  <?php endforeach ?>
                </li>
              <?php endif ?>
              <?php if (!empty($member->physical_address[0])): ?>
                <li class="contact-address">
                  <?php foreach($member->physical_address as $addr_line): ?>
                    <?php print $addr_line ?><br/>
                  <?php endforeach ?>
                </li>
              <?php endif; ?>
            </ul>

            <?php # Show linked list of depts (two deep) for everyone except head executive ?>
            <?php if($member->id != $head_executive->id): ?>
              <ul class="list-unstyled eg-responsibilities">
                <?php foreach($dept_tree as $d1): ?>

                  <?php # TODO: revisit this logic if we ever have an EG member who is not ?>
                  <?php # the head of a top-level department. ?>
                  <?php if($d1->head_person_id == $member->id): ?>
                    <li>
                      <a href="<?php print $GLOBALS['dept_path'] . $d1->slug; ?>"><?php print $d1->name; ?></a>
                      <?php if($d1->children): ?>
                        <ul class="list-unstyled">
                        <?php foreach($d1->children as $d2): ?>
                          <li><a class="dept-name" href="<?php print $GLOBALS['dept_path'] . $d2->slug; ?>"><?php print $d2->name; ?></a></li>
                        <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

          </div>
        </div>

      <?php endforeach; ?>

      <hr/>
    </div>

    <div class="col-sm-3 col-sm-pull-9">
      <?php # Pull in sidebar as partial template for reuse on EG page ?>
      <?php include(drupal_get_path('module', 'dul_staff').'/dept_sidebar_menu.tpl.php'); ?>
    </div>


  </div>
</div>
