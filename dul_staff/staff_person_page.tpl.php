<?php

/**
 * @file
 * Default theme implementation to display DUL person page
 *
 * Available variables:
 * - $person: stdClass Object for current person matched by id or slug
 */

?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<!-- uncomment print to inspect records in browser -->
<?php #print kpr($person, TRUE, "Current Person"); ?>

<div id="directory-main-content" class="container person-view" itemscope itemtype="http://schema.org/Person">

  <div class="row">

    <div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">
      <?php # Render the page title here in the main column. The default title at the ?>
      <?php # top of page is hidden via CSS, which we may want to revisit in future. ?>
      <div class="page-header">
        <h1 class="person-title" id="display-name" itemprop="name"><?php print $person->display_name ?></h1>
        <p class="person-job-title" itemprop="jobTitle"><?php print $person->preferred_title ? $person->preferred_title : $person->title; ?></p>

        <!-- # links to 3rd parties -->
        <div class="icon-links">

            <?php /* scholars@duke */

              $scholars_url = 'https://scholars.duke.edu/widgets/api/v0.9/people/contact/all.json?uri=' . $person->unique_id;

              function get_scholars_id($url) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                $data = curl_exec($ch);
                curl_close($ch);
                return $data;
              }

              $scholars_json = json_decode(get_scholars_id($scholars_url), true);

              if (!empty($scholars_json[0]["profileURL"])) {
                print '<div class="item"><a class="scholars-icon-link" href="' . $scholars_json[0]["profileURL"] . '" data-original-title="Scholars@Duke profile">Scholars@Duke Profile</a></div>';
              };

            ?>

            <?php /* orcid */
              if (!empty($person->orcid)) {
                print '<div class="item"><a class="orcid-icon-link" href="' . $person->orcid . '" data-original-title="ORCID profile">ORCID profile</a></div>';
              }
            ?>

        </div>
      
      </div>

      <div class="person-content">

        <?php 
          /* Get Scholars@Duke profile text as a fallback for DUL profile */
          $scholars_profile_text = "";
          if (!empty($scholars_json[0]["overview"])) {
            $scholars_profile_text = $scholars_json[0]["overview"];
          };

          /* DUL profile text overrides Scholars@Duke profile if present */
          $profile_text = !empty($person->profile) ? $person->profile : $scholars_profile_text

        ?>

        <!-- # Profile -->
        <?php if (!empty($profile_text)): ?>

          <section id="person-summary" class="expandable-content-wrapper">
            
            <div class="profile-content expandable-content" itemprop="description">
              <?php print $profile_text; ?>
            </div>
            
            <div class="expandable-content-controls">
              <span class='show-control more'><a href="javascript:void(0);" class="btn btn-sm btn-show">show more <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a></span>
              <span class='show-control less'><a href="#display-name" class="btn btn-sm btn-hide">show less <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a></span>
            </div>

          </section>

        <?php endif ?>

      </div>

      <div class="person-content row row-no-gutters">
        
        <div class="col-sm-6"><!-- left column -->

          <?php if (!empty($person->departments)): ?>
            <!-- # Departments -->
            <section class="departments">
              <h2>Department<?php echo (count((array)$person->departments) != 1 ? 's' : '') ?></h2>
                
                <ul>
                <?php
                  foreach($person->departments as $pd) {
                    echo '<li itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><a href="' . $GLOBALS['dept_path'] . $pd->slug . '" itemprop="url"><span itemprop="name">' . $pd->name . '</span></a></li>';
                  }
                ?>
                </ul>

            </section>
          <?php endif ?>

          <!-- # Subject Areas -->
          <?php if (!empty($person->subject_areas)): ?>
            <section class="subject_areas">
              <h2>Subject Area<?php echo (count((array)$person->subject_areas) != 1 ? 's' : '') ?></h2>
                
                <ul>
                <?php
                  foreach($person->subject_areas as $psa) {
                    echo '<li><a href="' . $GLOBALS['subject_area_path'] . '#' . $psa->id . '"><span itemprop="knowsAbout">' . $psa->title . '</span></a></li>';
                  }
                ?>
                </ul>

            </section>
          <?php endif ?>


          <!-- # Languages -->
          <?php if (!empty($person->languages)): ?>
            <section class="languages">
              <h2>Language Skill<?php echo (count((array)$person->languages) != 1 ? 's' : '') ?></h2>
                
                <ul>
                <?php
                  foreach($person->languages as $pl) {
                    echo '<li itemprop="knowsLanguage">' . $pl->name . '</li>';
                  }
                ?>
                </ul>

            </section>
          <?php endif ?>

          <!-- # Trainings -->
          <?php if (!empty($person->trainings)): ?>
            <section class="trainings">
              <h2>Trainings &amp; Certifications</h2>

                <ul class="list-unstyled">
                <?php foreach($person->trainings as $t): ?>
                  <li class="<?php print !empty($t->icon_url) ? 'custom-icon' : ''; ?>" style="background-image: url(<?php print !empty($t->icon_url) ? $t->icon_url : ''; ?>)">
                    <?php if (!empty($t->url)): ?>
                      <a href="<?php print $t->url; ?>">
                        <?php print $t->title; ?>
                      </a>
                    <?php else: ?>
                      <?php print $t->title; ?>
                    <?php endif ?>
                  </li>
                <?php endforeach ?>
                </ul>

            </section>
          <?php endif ?>

        </div> <!-- / end left column -->
        
        <div class="col-sm-6"> <!-- right column -->
          <!-- # Libguides -->
          <?php if (!empty($person->libguide_id)): ?>
            
            <section class="research-guides">

              <h2>Top Research Guides</h2>

              <script>
              springshare_widget_config_1 = { path: 'guides' };
              </script>
              <div id="s-lg-widget-1"></div>
              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://lgapi-us.libapps.com/widgets.php?site_id=353&widget_type=1&search_terms=&search_match=2&sort_by=count_hit&list_format=1&drop_text=Select+a+Guide...&output_format=1&load_type=2&enable_description=0&enable_group_search_limit=0&enable_subject_search_limit=0&account_ids%5B0%5D=<?php print $person->libguide_id; ?>&widget_title=Guide+List&widget_height=250&widget_width=100%25&widget_link_color=2954d1&widget_embed_type=1&num_results=5&enable_more_results=0&window_target=2&config_id=1";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","s-lg-widget-script-1");</script>
              
              <p><a class="btn btn-default btn-sm" href="https://guides.library.duke.edu/prf.php?account_id=<?php print $person->libguide_id; ?>">View all guides &raquo;</a></p>

            </section>

          <?php endif ?>
        </div> <!-- / end right column -->



      </div>

    </div>

    <div class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9">
      <div class="contact-wrap">
        <?php dul_staff_person_photo($person, 'staff_dir_large'); ?>

        <?php if (!empty($person->libcal_id)): ?>
          <a class="scheduler-button btn btn-primary" href="<?php print $GLOBALS['libcal_reservation_path'] . $person->libcal_id; ?>" title="Schedule an Appointment"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>
   &nbsp;Schedule an Appointment</a>
        <?php endif ?>

        <div class="contact">

          <ul class="contact-info list-unstyled">
            <?php if (!empty($person->email) ): //&& $person->email_privacy == '0' ?>
              <li class="contact-email" itemprop="email" title="<?php print $person->email ?>"><a href="mailto:<?php print $person->email ?>"><?php print $person->email ?></a></li>
            <?php endif ?>
            <?php if (!empty($person->phone)): ?>
              <li class="contact-phone" itemprop="telephone">
                <?php print $person->phone ?>
              </li>
            <?php endif ?>
          </ul>

          <hr />

          <?php if (!empty($person->pronouns)): ?>
            <ul class="contact-info list-unstyled">
              <li class="pronouns">
                <span class="contact-label">Pronouns</span><br />
                <?php print $person->pronouns; ?>
              </li>
            </ul>

            <hr />
          <?php endif ?>

          <ul class="contact-info contact-addresses list-unstyled">
            <?php if (!empty($person->campus_box)): ?>

              <?php // # format campus box
                $cleaned_campus_box = str_replace('$', '<br />', $person->campus_box);
                $cleaned_campus_box = str_replace('277080193', '27708-0193', $cleaned_campus_box);
              ?>

              <li class="contact-box">
                <span class="contact-label">Mailing Address</span><br />
                <span itemprop="address"><?php print $cleaned_campus_box; ?></span>
              </li>
            <?php endif ?>
            <?php if (!empty($person->physical_address)): ?>

              <?php // # format campus box
                $cleaned_physical_address = str_replace('$', '<br />', $person->physical_address);
                $cleaned_physical_address = str_replace('277080193', '27708-0193', $cleaned_physical_address);
              ?>

              <li class="contact-address" itemprop="workLocation" itemscope itemtype="http://schema.org/Place">
                <span class="contact-label">Location</span><br />
                <span itemprop="address"><?php print $cleaned_physical_address; ?></span>
              </li>
            <?php endif; ?>
          </ul>
        </div>
      </div>


    </div>

  </div>

</div>
