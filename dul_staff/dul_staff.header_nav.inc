<?php 

/* ++ dynamic lookup for staff names ++ */

// need autocomplete library
drupal_add_library('system', 'ui.autocomplete');

// build array of all staff display names
$all_staff_names = dul_staff_directory_all_staff_names();
$all_department_names = dul_staff_directory_all_department_names();
$all_names_combined = array_merge($all_staff_names, $all_department_names);

// do JS inline below for ease of getting all_staff_names array
?>

<script type="text/javascript">
(function($) {
  $(document).ready(function(){
    var $all_names_combined = <?php echo json_encode($all_names_combined); ?>;
    $( "#name" ).autocomplete({
      source: $all_names_combined,
      select: function(event, ui) {
        if(ui.item){
          $(event.target).val(ui.item.value);
        }
        $(event.target.form).submit();
      }
    });
  });
})(jQuery);
</script>


<!-- uncomment print to inspect records in browser -->
<?php #print kpr($people, TRUE); ?>
<?php #print kpr($abc_nav, TRUE); ?>

<!-- Outer container -->
<div class="container-fluid">
  <div class="skip-link">
    <a href="#directory-main-content" class="sr-only sr-only-focusable">Skip directory navigation</a>
  </div>

  <nav class="row staff-directory-nav" aria-label="Staff Directory Main Navigation" role="navigation">

        <div class="browse-buttons col-md-8">

          <a class="btn btn-link" href="<?php print $GLOBALS['dept_path']; ?>">Departments</a>
          <a class="btn btn-link" href="<?php print $GLOBALS['browse_path']; ?>a">Staff A-Z</a>
          <a class="btn btn-link" href="<?php print $GLOBALS['subject_specialists_path']; ?>">Subject Specialists</a>
          <a class="btn btn-link" href="<?php print $GLOBALS['executive_group_path']; ?>">Executive Group</a>

        </div>

        <div class="staff-search col-md-4">
          <form name="staff-search" id="staff-search-form" action="<?php print $GLOBALS['search_path']; ?>" method="get">
            <div class="form-group">
              <div class="input-group">
                <label for="staff-names-search" class="sr-only">Search by name:</label>
                <input class="form-control" placeholder="Search by name..." name="name" id="name" aria-label="Search staff by name">
                <span class="input-group-btn">
                  <button class="btn btn-link" type="submit" id="search" aria-label="Search">
                    <span class="sr-only">Submit</span>
                    <i class="fa fa-search" aria-hidden="true"></i>
                  </button>
                </span>
              </div>
            </div>
          </form>
        </div>

  </nav>
</div>