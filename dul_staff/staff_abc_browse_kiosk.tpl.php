<?php

/**
 * @file
 * Implementation for kiosk content, based on default theme to display a grid of staff members with last name
 * beginning with a given letter.
 *
 * Available variables:
 * - $staff_members: array of stdClass Objects; all viewable staff for this view, A-Z
 * - $abc_nav: array of strings: each letter that should be clickable in A-Z nav
 */
?>

<!-- uncomment print to inspect records in browser -->
<?php #print kpr($staff_members, TRUE, "Staff Members"); ?>

<?php # Pull in A-Z letter browse partial template ?>
<?php include(drupal_get_path('module', 'dul_staff') . '/abc_names_nav_kiosk.tpl.php'); ?>

<div id="directory-main-content" class="container abc-view">
  
  <div class="row">

    <div class="col-sm-12">
      
      <?php $people = $staff_members ?>
      <?php include_once 'people_grid_kiosk.tpl.php'; ?>

      <hr/>

    </div>

  </div>
</div>
