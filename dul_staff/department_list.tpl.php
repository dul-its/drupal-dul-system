<?php

/**
 * @file
 * Partial reusable template to render departments in a list
 */
?>


<ul class="spacing-sm list-styled">

<?php foreach($departments as $dept): ?>

<?php // fallback to ID if slug is not present
  if ($dept->slug) {
    $slugLink = $dept->slug;
  } else {
    $slugLink = $dept->id;
  }
?>

  <li>
    <a href="<?php print $GLOBALS['dept_path'] . $slugLink; ?>">
      <?php print $dept->name; ?>
    </a>
  </li>

<?php endforeach; ?>

</ul>