<?php

function dul_staff_directory() {
  $abc_nav = dul_staff_directory_abc_nav();
  $staff_members = dul_staff_directory_all_staff();

  #print kpr($abc_nav_active_letters, TRUE);
	db_set_active('dul_directory');

	$result = db_query('SELECT d.* FROM {departments} d ORDER BY d.name');
	$all_departments = $result->fetchAll();

	$result = db_query('SELECT * from {departments} where ancestry IS NULL order by position');
	$top_dept = $result->fetchAll();

	$result = db_query('SELECT * FROM {people} WHERE viewable = 1 ORDER BY last_name, first_name');
	$people = $result->fetchAll();

	# restore the database connection back to the Drupal-core tables
	db_set_active();

	return array(
		'#theme' => 'staff_directory_page',
		'#staff_members' => $staff_members,
		'#abc_nav' => $abc_nav,
	);
}



/**
 * Page callback for All Departments Page (about/directory/dept)
 */
function dul_staff_all_depts() {
  $dept_tree = dul_staff_directory_dept_tree();
  $dept_list = dul_staff_directory_dept_list();
  /* send blank $dept object over so the sidebar nav will work */
  $dept = new stdClass();
  $dept->id = '';
  $dept->ancestor_ids = array();

  # These variables must all be specified in dul_staff_theme() in dul_staff.module.
  return array(
    '#theme' => 'staff_all_depts',
    '#dept_tree' => $dept_tree,
    '#dept_list' => $dept_list,
    '#dept' => $dept
  );

}

/**
 * Page callback for Executive Group Page (about/directory/executive-group)
 */
function dul_staff_executive_group() {
  $dept_tree = dul_staff_directory_dept_tree();
	db_set_active('dul_directory');

  # Get the head executive (person who is head of dept 1)
	$result = db_query(
	'SELECT p.* FROM {people} p '
	. 'INNER JOIN {roles} r ON p.id = r.person_id '
	. 'INNER JOIN {departments} d ON d.head_person_id = r.person_id '
	. 'WHERE r.department_id = :top_dept_id',
	  array(':top_dept_id' => 1)
	);
	$head_executive = $result->fetchObject();

  # Get all staff who are EG members
	$result = db_query(
	'SELECT p.* FROM {people} p '
	. 'WHERE p.is_eg_member = true '
	. 'ORDER BY p.last_name, p.first_name',
	  array(':head_executive_id' => $head_executive->id)
	);
	$eg_members_with_dupes = $result->fetchAll();

  # If there's a head executive, put that person first
  if (!empty($head_executive)) {
    array_unshift($eg_members_with_dupes, $head_executive);
  }

  # De-duplicate head executive from list of EG members
  $eg_members = array();
  foreach ($eg_members_with_dupes as $obj) {
    $eg_members[$obj->id] = $obj;
  }



  # Make dept an empty object so EG page plays nicely with the dept page sidebar
  # EG isn't a department in our DB, so this is a way to get the sidebar to show
  # on the EG page; it needs a dept present to know what collapsed/active context
  # to present.
  $dept = new stdClass();
  $dept->id = 0;
  $dept->ancestor_ids = array();

  # Split the campus box & physical address values into arrays on $
  # So they can be presented with line breaks in the templates.
  foreach ($eg_members as $member) {
    $member->campus_box = array_map('trim', explode('$', $member->campus_box));
    $member->physical_address = array_map('trim', explode('$', $member->physical_address));
  }


	# restore the database connection back to the Drupal-core tables
	db_set_active();

  # These variables must all be specified in dul_staff_theme() in dul_staff.module.
  return array(
    '#theme' => 'executive_group',
    '#head_executive' => $head_executive,
    '#eg_members' => $eg_members,
    '#dept_tree' => $dept_tree,
    '#dept' => $dept
  );

}


/**
 * Page callback for Department Page (about/directory/dept/*)
 */
function dul_staff_dept(stdClass $dept, $selector=NULL) {
	$dept_tree = dul_staff_directory_dept_tree();
	db_set_active('dul_directory');

  # Uncomment to troubleshoot
  # print kpr($dept, TRUE, "This Department");

  $dept_id = $dept->id;
  $dept->ancestor_ids = explode("/", $dept->ancestry);

  # This sets the page title for the html <title> but we're
  # removing it from the page rendering in order to have more
  # flexibility in how/where it displays, e.g., so it appears
  # to the right of the sidebar nav on a Department page.
  # See dul_staff.module > dul_staff_preprocess_page()
  drupal_set_title($dept->name);
  
  

  # Return department head
  # ------------------------------------------------------
	$result = db_query(
	'SELECT p.* FROM {people} p '
	. 'WHERE p.id = :head_person_id '
	. 'AND p.viewable = 1 ',

	  array(':head_person_id' => $dept->head_person_id)
	);
	$dept_head = $result->fetchObject();
  # print kpr($dept_head, TRUE, "Dept Head");


  # Return all depts that are descendants of this department
  # ------------------------------------------------------
	$result = db_query(
	'SELECT d.* FROM {departments} d '
	. 'WHERE d.ancestry = :dept_id '
	. 'OR d.ancestry LIKE :dept_match_1 '
	. 'OR d.ancestry LIKE :dept_match_2 '
	. 'OR d.ancestry LIKE :dept_match_3 '
	. 'ORDER BY d.ancestry, d.position',

	  array(':dept_id' => $dept->id,
	        ':dept_match_1' => ('%/' . $dept->id ),
	        ':dept_match_2' => ('%/' . $dept->id . '/%' ),
	        ':dept_match_3' => ( $dept->id . '/%' )
	        )
	);

	$dept_descendants = $result->fetchAll();
  # print kpr($dept_descendants, TRUE, "Descendant Depts");


  # Get IDs of all dept heads & asst. heads in this dept and
  # its descendant departments.
  # ------------------------------------------------------
  $manager_tree_ids = array();
  if (isset($dept->head_person_id)) {
    $manager_tree_ids[] = $dept->head_person_id;
  }
  if (isset($dept->asst_head_person_id)) {
    $manager_tree_ids[] = $dept->asst_head_person_id;
  }
  foreach($dept_descendants as $descendant) {
    if (isset($descendant->head_person_id)) {
      $manager_tree_ids[] = $descendant->head_person_id;
    }
    if (isset($descendant->asst_head_person_id)) {
      $manager_tree_ids[] = $descendant->asst_head_person_id;
    }
  }
  # print kpr($manager_tree_ids, TRUE, "Manager Tree IDs");

  # Return all the people who are heads or asst. heads of this
  # dept or any of its descendant departments
  # ----------------------------------------------------------

	$result = db_query(
	'SELECT p.* FROM {people} p '
	. 'WHERE p.id IN (:manager_tree_ids) '
	. 'AND p.viewable = 1 '
	. 'ORDER BY FIELD(id,:manager_tree_ids)',
	  array(':manager_tree_ids' => $manager_tree_ids ?: array(''))
	        # default empty string array needed for the IN operator to work
	);
	$manager_tree_people_with_dupes = $result->fetchAll();
  # print kpr($manager_tree_people_with_dupes, TRUE, "Manager Tree People With Dupes");

  $dept_descendant_ids = array();
  foreach($dept_descendants as $descendant) {
    $dept_descendant_ids[] = $descendant->id;
  }
  # print kpr($dept_descendant_ids, TRUE, "Descendant Dept IDs");


  # Return all the people who are in this department or any of
  # its descendant departments
  # ----------------------------------------------------------

  $result = db_query(
  'SELECT p.* FROM {people} p '
  . 'INNER JOIN {roles} r ON p.id = r.person_id '
  . 'WHERE (r.department_id = :dept_id '
  . 'OR r.department_id IN (:dept_deep_ids)) '
  . 'AND p.viewable = 1 '
  . 'ORDER BY last_name, first_name',
    array(':dept_id' => $dept->id,
          ':dept_deep_ids' => $dept_descendant_ids ?: array(''))
          # default empty string array needed for the IN operator to work
  );
  $dept_members_with_dupes = $result->fetchAll();

  # Put the managers in front.
  $dept_managers_and_members_with_dupes = array_merge(
    $manager_tree_people_with_dupes, $dept_members_with_dupes
  );

  # De-duplicate dept heads and anyone else who is in multiple depts
  $dept_members = array();
  foreach ($dept_managers_and_members_with_dupes as $obj) {
      $dept_members[$obj->id] = $obj;
  }

	# restore the database connection back to the Drupal-core tables
	db_set_active();

  # Set some Open Graph & Twitter Card tags for the Dept in the html <head>.

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:type',
      'content' => 'website',
    ),
  );
  drupal_add_html_head($element, 'og_type');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:url',
      'content' => 'https://library.duke.edu' . $GLOBALS['dept_path'] . ( $dept->slug ? $dept->slug : $dept->id )
    ),
  );
  drupal_add_html_head($element, 'og_url');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' => $dept->name,
    ),
  );
  drupal_add_html_head($element, 'og_title');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:site_name',
      'content' => 'Duke University Libraries',
    ),
  );
  drupal_add_html_head($element, 'og_site_name');

  /* Cleanup and truncate description html before writing to og:description */
  $desc_orig = html_entity_decode($dept->description);
  $desc_tagless = strip_tags($desc_orig);
  $desc_clean = trim($desc_tagless);
  /* Truncate to 400 char but don't split words */
  if( strlen($desc_tagless) > 400) {
    $str = explode( "\n", wordwrap( $desc_tagless, 400));
    $desc_clean = $str[0] . '...';
  }
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' => $desc_clean,
    ),
  );
  drupal_add_html_head($element, 'og_description');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:card',
      'content' => 'summary'
    ),
  );
  drupal_add_html_head($element, 'twitter_card');

  # These variables must all be specified in dul_staff.module > dul_staff_theme().
	return array(
		'#theme' => 'staff_department_page',
		'#dept' => $dept,
		'#dept_tree' => $dept_tree,
		'#dept_members' => $dept_members,
	);

}


/**
 * Page callback for Person Page (about/directory/staff/*)
 */
function dul_staff_person(stdClass $person, $selector=NULL) {

  db_set_active('dul_directory');

  # Uncomment to troubleshoot
  # print kpr($person, TRUE, "This Person");

// # Get a person's department(s)

  // get department_ids from role table
  $result = db_query( 'SELECT department_id from {roles} where person_id = :person_id', array(':person_id' => $person->id) );
	$result_department_ids = $result->fetchAll();

  if (count($result_department_ids) > 0) {

    $person_department_ids_array = array();

    // add contents of results object to an array
    foreach ($result_department_ids as $dept_id) {
      array_push($person_department_ids_array, $dept_id->department_id);
    }

    // query departments using the array
    $result = db_query( 'SELECT * FROM {departments} WHERE "id" IN (:pdids)', array(':pdids' => $person_department_ids_array));
    $result_departments_names = $result->fetchAll();
    
    // send values to template
    $person->departments = $result_departments_names;
    
  }


// # Get a person's subject(s)

  // get subject_area_ids from expertise table
  $result = db_query( 'SELECT subject_area_id from {expertise} where person_id = :person_id', array(':person_id' => $person->id) );
	$result_subject_area_ids = $result->fetchAll();

  if (count($result_subject_area_ids) > 0) {

    $person_subject_area_ids_array = array();

    // add contents of results object to an array
    foreach ($result_subject_area_ids as $subj_id) {
      
      // keep unique
      if(!in_array($subj_id->subject_area_id, $person_subject_area_ids_array, true)){
        array_push($person_subject_area_ids_array, $subj_id->subject_area_id);
      }
      
    }

    // query subject_areas using the array
    $result = db_query( 'SELECT * FROM {subject_areas} WHERE "id" IN (:psaids)', array(':psaids' => $person_subject_area_ids_array));
    $result_subject_area_names = $result->fetchAll();
    
    // send values to template
    $person->subject_areas = $result_subject_area_names;

  }


// # Get a person's language(s)

  // get language_ids from fluencies table
  $result = db_query( 'SELECT language_id from {fluencies} where person_id = :person_id', array(':person_id' => $person->id) );
	$result_language_ids = $result->fetchAll();

  if (count($result_language_ids) > 0) {

    $person_language_ids_array = array();

    // add contents of results object to an array
    foreach ($result_language_ids as $lang_id) {
      
      // keep unique
      if(!in_array($lang_id->language_id, $person_language_ids_array, true)){
        array_push($person_language_ids_array, $lang_id->language_id);
      }
      
    }

    // query languages using the array
    $result = db_query( 'SELECT * FROM {languages} WHERE "id" IN (:plids)', array(':plids' => $person_language_ids_array));
    $result_language_names = $result->fetchAll();
    
    // send values to template
    $person->languages = $result_language_names;

  }

  // # Get a person's trainings/certifications

  $result = db_query(
  'SELECT DISTINCT t.* FROM {trainings} t, {certifications} c '
  . 'WHERE c.person_id = :person_id AND c.training_id = t.id '
  . 'AND t.active = true '
  . 'ORDER BY t.title',
    array(':person_id' => $person->id)
  );
  foreach($result->fetchAll() as $t) {
    $person->trainings[] = $t;
  }

  # restore the database connection back to the Drupal-core tables
  db_set_active();

  # This sets the page title for the html <title> but we're
  # removing it from the page rendering in order to have more
  # flexibility in how/where it displays, e.g., so it appears
  # to the right of the sidebar nav on a Department page.
  # See dul_staff.module > dul_staff_preprocess_page()
  drupal_set_title($person->display_name);

  # Set Open Graph & Twitter Card tags for the Person Profile in the html <head>.

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:type',
      'content' => 'website',
    ),
  );
  drupal_add_html_head($element, 'og_type');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:url',
      'content' => 'https://library.duke.edu' . $GLOBALS['staff_profile_path'] . ( $person->slug ? $person->slug : $person->id )
    ),
  );
  drupal_add_html_head($element, 'og_url');



  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' => $person->display_name,
    ),
  );
  drupal_add_html_head($element, 'og_title');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:site_name',
      'content' => 'Duke University Libraries',
    ),
  );
  drupal_add_html_head($element, 'og_site_name');

  /* Cleanup and truncate profile html before writing to og:description */
  $desc_orig = html_entity_decode($person->profile);
  $desc_tagless = strip_tags($desc_orig);
  $desc_clean = trim($desc_tagless);
  /* Truncate to 400 char but don't split words */
  if( strlen($desc_tagless) > 400) {
    $str = explode( "\n", wordwrap( $desc_tagless, 400));
    $desc_clean = $str[0] . '...';
  }
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' => $desc_clean,
    ),
  );
  drupal_add_html_head($element, 'og_description');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image',
      'content' => dul_staff_person_photo_src($person, 'staff_dir_thumb')
    ),
  );
  drupal_add_html_head($element, 'og_image');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:image',
      'content' => dul_staff_person_photo_src($person, 'staff_dir_thumb')
    ),
  );
  drupal_add_html_head($element, 'twitter_image');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image:alt',
      'content' => $person->display_name
    ),
  );
  drupal_add_html_head($element, 'og_image_alt');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image:type',
      'content' => 'image/jpeg'
    ),
  );
  drupal_add_html_head($element, 'og_image_type');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image:width',
      'content' => '300'
    ),
  );
  drupal_add_html_head($element, 'og_image_width');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image:height',
      'content' => '400'
    ),
  );
  drupal_add_html_head($element, 'og_image_height');

  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'twitter:card',
      'content' => 'summary'
    ),
  );
  drupal_add_html_head($element, 'twitter_card');

  # These variables must all be specified in dul_staff.module > dul_staff_theme().
	return array(
		'#theme' => 'staff_person_page',
		'#person' => $person,
	);

}


/**
 * Page callback for A-Z Letter Browse Page (about/directory/browse/*)
 */
function dul_staff_abc_browse($letter, $selector=NULL) {
  if ($selector == 'title') {
    return "Browse Staff: " . ucfirst($letter);
  }

  $starting_letter = $letter;
  $abc_nav = dul_staff_directory_abc_nav();

  db_set_active('dul_directory');

  if($letter == 'all') {
    $result = db_query(
    'SELECT p.* FROM {people} p '
    . 'WHERE p.viewable = 1 '
    . 'ORDER BY p.last_name, p.first_name, p.middle_name',
      array(':name_match' => ($starting_letter . '%'))
    );
  } elseif (preg_match('/^[a-zA-Z]$/',$letter)) {
    $result = db_query(
    'SELECT p.* FROM {people} p '
    . 'WHERE p.viewable = 1 '
    . 'AND p.last_name LIKE :name_match '
    . 'ORDER BY p.last_name, p.first_name, p.middle_name',
      array(':name_match' => ($starting_letter . '%'))
    );
  } else {
    #TODO: figure out how to throw a usable error page
    #return drupal_not_found();
    #return MENU_NOT_FOUND;
  }

  $staff_members = $result->fetchAll();

  db_set_active();

  # These variables must all be specified in dul_staff.module > dul_staff_theme().
  return array(
    '#theme' => 'dul_staff_abc_browse',
    '#staff_members' => $staff_members,
    '#abc_nav' => $abc_nav,
    '#starting_letter' => $letter
  );

}




/**
 * Page callback for [Kiosk] A-Z Letter Browse Page (about/directory/kiosk/*)
 */
function dul_staff_abc_browse_kiosk($letter, $selector=NULL) {

  $abc_nav = dul_staff_directory_abc_nav();

  db_set_active('dul_directory');

  /* get all staff for kiosk display */
  $result = db_query(
    'SELECT p.* FROM {people} p '
    . 'WHERE p.viewable = 1 '
    . 'ORDER BY p.last_name, p.first_name, p.middle_name',
      array(':name_match' => ($starting_letter . '%'))
  );

  $staff_members = $result->fetchAll();

  db_set_active();

  # These variables must all be specified in dul_staff.module > dul_staff_theme().
  return array(
    '#theme' => 'dul_staff_abc_browse_kiosk',
    '#staff_members' => $staff_members,
    '#abc_nav' => $abc_nav,
  );

}



/**
 * Page callback for Subject Specialists Page (about/directory/subject-specialists)
 */
function dul_staff_subject_specialists() {

  db_set_active('dul_directory');

  $subject_tree = array();

  # Get all the subjects that have people and/or external contact pts assigned
  # and disregard the rest.
  $result = db_query(
  'SELECT s.* FROM {subject_areas} s, {expertise} e, {external_expertise} ex '
  . 'WHERE s.id = e.subject_area_id OR s.id = ex.subject_area_id '
  . 'GROUP BY s.id '
  . 'ORDER BY s.title '
  );
  $subjects_covered = $result->fetchAll();

  foreach ($subjects_covered as $s) {

    # Get DUL staff subject specialists for the subject
    $result = db_query(
    'SELECT DISTINCT p.* FROM {people} p, {expertise} e '
    . 'WHERE e.subject_area_id = :subject_id AND e.person_id = p.id '
    . 'AND p.viewable = true '
    . 'ORDER BY p.last_name, p.first_name, p.middle_name',
      array(':subject_id' => $s->id)
    );
    foreach($result->fetchAll() as $p) {
      $s->staff_specialists[] = $p;
    }

    # Get external contact points for the subject
    $result = db_query(
    'SELECT ec.* FROM {external_contacts} ec, {external_expertise} ex '
    . 'WHERE ex.subject_area_id = :subject_id AND ex.external_contact_id = ec.id',
      array(':subject_id' => $s->id)
    );
    foreach($result->fetchAll() as $ec) {
      $s->external_specialists[] = $ec;
    }

    $subject_tree[] = $s;
  }

  #print kpr($subject_tree, TRUE);

  # restore the database connection back to the Drupal-core tables
  db_set_active();

  # These variables must all be specified in dul_staff_theme() in dul_staff.module.
  return array(
    '#theme' => 'dul_staff_subject_specialists',
    '#subject_tree' => $subject_tree,
  );

}