<?php

/**
 * @file
 * Partial reusable template to render people in a grid
 */
?>


<?php foreach($people as $person): ?>

<?php // fallback to ID if slug is not present
  if ($person->slug) {
    $slugLink = $person->slug;
  } else {
    $slugLink = $person->id;
  }
?>

  <div class="person-wrap" itemprop="employee" itemscope itemtype="https://schema.org/Person">
    <a href="<?php print $GLOBALS['staff_profile_path'] . $slugLink; ?>" aria-label="<?php print $person->display_name; ?>" itemprop="url">
      <?php dul_staff_person_photo($person, 'staff_dir_thumb'); ?>
    </a>
    <div class="person-name" itemprop="name">
      <a href="<?php print $GLOBALS['staff_profile_path'] . $slugLink; ?>">
        <?php print $person->display_name; ?>
      </a>
    </div>
    <div class="person-job-title" itemprop="jobTitle"><?php print $person->preferred_title ? $person->preferred_title : $person->title; ?></div>
  </div>

<?php endforeach; ?>
