<?php

/**
 * @file
 * Default theme implementation to display DUL Staff Directory Search
 *
 * Available variables:
 */
?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

    
    <?php if ( !$theSearchString ): ?>
      
      <p class="lead"><em>Please enter a search term</em></p>
    
    <?php else: ?>
      
      <?php if ($result_name_search || $result_department_search): ?>

        <?php if ($result_name_search): ?>

          <h2>Staff Matching Your Search</h2>

          <div id="directory-main-content" class="people-grid">
            <?php $people = $result_name_search ?>
            <?php include_once 'people_grid.tpl.php'; ?>
            <div class="person-wrap">
              <a class="browse-more-people" href="<?php print $GLOBALS['browse_path']; ?>all">
                All Staff A-Z &raquo;
              </a>
            </div>
          </div>

        <?php endif ?>

        <?php if ($result_department_search): ?>

          <h2>Departments Matching Your Search</h2>

          <div class="department-results-list">
            <?php $departments = $result_department_search ?>

            <?php include_once 'department_list.tpl.php'; ?>
            
            <p><a class="browse-more-departments" href="<?php print $GLOBALS['dept_path']; ?>">Browse All Departments &raquo;</a></p>
            
          </div>

        <?php endif ?>
      
      <?php else: ?>

        <p class="lead">No results found for <em><?php print $theSearchString ?></em></p>
        
      <?php endif ?>

    <?php endif ?>


</div> <!-- closes Outer container -->
