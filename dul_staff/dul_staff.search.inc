<?php

function dul_staff_directory_search() {
  
  $staff_members = dul_staff_directory_all_staff();
  $department_names = dul_staff_directory_all_departments();

  // clean the input
  $theSearchString = preg_replace("/[^-.&,A-Za-z0-9() ]/", '', $_GET['name']);

  //print kpr($theSearchString, TRUE, "Search String");


  if ($theSearchString) {

    db_set_active('dul_directory');
    
    // check for exact match in Staff
    $result = db_query(
      'SELECT * FROM {people} '
      . 'WHERE viewable=1 '
      . 'AND display_name = :name_match '
      . 'ORDER BY display_name DESC',
        array(':name_match' => ($theSearchString) )
      );
    
    if ( $result->rowCount() > 0 ) {
      $result_name_search = $result->fetchAll();
    } else {
    // do a like search in Staff
      $result = db_query(
      'SELECT * FROM {people} '
      . 'WHERE viewable=1 '
      . 'AND (first_name LIKE :name_search '
      . 'OR last_name LIKE :name_search '
      . 'OR display_name LIKE :name_search) '
      . 'ORDER BY display_name DESC, first_name ASC, first_name ASC', 
        array(':name_search' => ('%' . $theSearchString . '%'))
      );
      $result_name_search = $result->fetchAll();
    }

    // check for exact match in Departments
    $result = db_query(
      'SELECT * FROM {departments} '
      . 'WHERE name = :name_match '
      . 'ORDER BY name DESC',
        array(':name_match' => ($theSearchString) )
      );
    
    if ( $result->rowCount() > 0 ) {
      $result_department_search = $result->fetchAll();
    } else {
    // do a like search in Departments
      $result = db_query(
      'SELECT * FROM {departments} '
      . 'WHERE name LIKE :name_search '
      . 'ORDER BY name DESC', 
        array(':name_search' => ('%' . $theSearchString . '%'))
      );
      $result_department_search = $result->fetchAll();
    }

  }

  // print kpr($result_name_search, TRUE, "People");
  // print kpr($result_department_search, TRUE, "Departments");


  # restore the database connection back to the Drupal-core tables
  db_set_active();

  # These variables must all be specified in staff_directory_search() in dul_staff.module.
  return array(
    '#theme' => 'staff_directory_search',
    '#staff_members' => $staff_members,
    '#department_names' => $department_names,
    '#theSearchString' => $theSearchString,
    '#result_name_search' => $result_name_search,
    '#result_department_search' => $result_department_search,
  );

}