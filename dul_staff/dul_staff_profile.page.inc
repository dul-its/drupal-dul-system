<?php

function dul_staff_profile_form($form, &$form_state, $uid) {

  // For local dev w/o shib, uncomment & replace ... with a netid:
  //$_SERVER['uid'] = 'netid';

	// Allow admins to edit via netID (by url param 'netid' : /user/[uid]/staff-profile?netid=[desired-netid])
	if (user_has_role(3)) {
		
		$params = drupal_get_query_parameters();

		if ( count(drupal_get_query_parameters()) > 0 ) {

			// remove cruft
			$netid_param = preg_replace('/[^-a-zA-Z0-9_]/', '', $params['netid']);

			// check that netid exists
			$netid_result = db_query( 'SELECT * FROM {shib_authmap} WHERE targeted_id LIKE :netid', array(':netid' => $netid_param . '%') );

			$netid_check = (array) $netid_result->fetchObject();

			// if there's stuff in the array...
			if (count($netid_check) > 1) {
				$_SERVER['uid'] = $netid_param;
			}

		}

	}


	# if $_SERVER['uid'] exists, then load the user's data from dul_directory.{people}
	if (!$_SERVER['uid'] || $_SERVER['uid'] == '') {

		return array(
			'bad-user' => array(
				'#markup' => t('Unable to determine the NetID from which to load directory information.')
			)
		);

	}

	db_set_active('dul_directory');
	$result = db_query(
		'SELECT p.* FROM {people} p '
		. 'WHERE net_id = :netid',
		array(':netid' => $_SERVER['uid']));
	
	$person = $result->fetchObject();

	// get subject_areas
	$sresult = db_query(
		'SELECT sa.* FROM {subject_areas} sa ORDER BY title');
	$subject_areas = array();
	foreach ($sresult as $srecord) {
		$subject_areas[$srecord->id] = $srecord->title;
	}

	// get a person's expertise
	$eresult = db_query(
		'SELECT e.id, e.person_id, e.subject_area_id FROM {expertise} e '
		. 'WHERE e.person_id = :person_id',
		array(':person_id' => $person->id)
	);
	$default_expertise_array = array();
	foreach ($eresult as $erecord) {
		array_push($default_expertise_array, $erecord->subject_area_id);
	}

	// get languages
	$lresult = db_query(
		'SELECT la.* FROM {languages} la ORDER BY name');
	$languages = array();
	foreach ($lresult as $lrecord) {
		$languages[$lrecord->id] = $lrecord->name;
	}

	// get a person's fluencies
	$fresult = db_query(
		'SELECT f.id, f.person_id, f.language_id FROM {fluencies} f '
		. 'WHERE person_id = :person_id',
		array(':person_id' => $person->id)
	);
	$default_fluencies_array = array();
	foreach ($fresult as $frecord) {
		array_push($default_fluencies_array, $frecord->language_id);
	}

  // get active trainings
  // NOTE: revisit whether the Drupal form should display inactive trainings,
  // i.e., should in/active only impact the public display?
  $tresult = db_query(
    'SELECT t.* FROM {trainings} t '
    . 'WHERE t.active = 1 '
    . 'ORDER BY title'
  );
  $trainings = array();
  foreach ($tresult as $trecord) {
          $trainings[$trecord->id] = $trecord->title;
  }

  // get a person's active certifications/trainings
  $cresult = db_query(
          'SELECT c.id, c.person_id, c.training_id FROM {certifications} c, {trainings} t '
          . 'WHERE c.person_id = :person_id '
          . 'AND c.training_id = t.id '
          . 'AND t.active = 1',
          array(':person_id' => $person->id)
  );
  $default_certifications_array = array();
  foreach ($cresult as $crecord) {
          array_push($default_certifications_array, $crecord->training_id);
  }

	// reset DB
	db_set_active();

	$form['welcome'] = array(
		'#markup' => t('<h2>Your Staff Profile</h2>')
	);

		
	if (!is_null($person)) {

		$form['submit_1'] = array(
			'#type' => 'submit',
			'#value' => t("Save Changes"),
      '#attributes' => array('class' => array('btn btn-success')),
      '#weight' => 3,
		);
		# ===========================================================
		# Staff self-managed fields: display as an expanded group
		# ===========================================================
		
		// dpm($form);

		$form['staff_data'] = array(
			'#type' => 'fieldset',
			'#title' => t('Staff Directory Info: ' . $person->display_name),
			'#description' => t('<p>This information in this form will appear in your <a href="https://library.duke.edu/about/directory/">directory listing on the DUL website</a></p>'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
			'#weight' => 2,
		);

		$form['staff_data']['photo'] = array(
			'#process' => array('custom_managed_file_element_process'),
			'#title' => t('Staff Photo'),
			'#type' => 'managed_file',
			'#description' => t('Upload your staff profile image. Images should be in portrait orientation and at least 600x800 pixels.'),
			'#default_value' => $person->photo_id,
			'#upload_validators' => array(
				'file_validate_is_image' => array(),
				'file_validate_image_resolution' => array('6000x8000','600x800'),
			),
			'#upload_location' => 'public://staff_images/',
			'#progress_indicator' => 'bar',
			'#progress_message' => 'Saving your file...',
			'#theme' => 'image_widget',
			'#attached' => array(
				'css' => array(
					'image-preview' => drupal_get_path('module', 'image') . '/image.css',
				),
			),
		);

		
		$form['staff_data']['preferred_title'] = array(
			'#type' => 'textfield',
			'#default_value' => $person->preferred_title,
			'#title' => t('Preferred Title'),
			'#description' => t('Your preferred job title. If left blank, the default HR value - <em>' . $person->title . '</em> will be displayed.'),
		);
		
		$form['staff_data']['profile'] = array(
			'#type' => 'text_format',
			'#default_value' => $person->profile,
			'#title' => t('Profile'),
			'#rows' => 10,
			'#description' => t('Brief description of your role, activities, and/or background. If you have a <a href="https://scholars.duke.edu/">Scholars@Duke</a> profile and prefer to use its profile text, leave this blank.')
		);
		
		$form['staff_data']['expertise'] = array(
			'#type' => 'select',
			'#title' => t("Subject Areas"),
			'#options' => $subject_areas,
			'#default_value' => $default_expertise_array,
			'#description' => t('Ctrl-click or Cmd-click to select more than one.'),
			'#size' => 10,
			'#multiple' => TRUE,
			'#chosen' => TRUE, // relies on chosen module: https://www.drupal.org/project/chosen
		);

		$form['staff_data']['pronouns'] = array(
			'#type' => 'textfield',
			'#default_value' => $person->pronouns,
			'#title' => t('Personal Pronouns'),
			'#description' => t('Examples of personal pronouns include but are not limited to:<br/>She / Her / Hers<br/>They / Them / Theirs<br/>He / Him / His<br/>Xe / Xem / Xyrs'),
		);

		$form['staff_data']['fluencies'] = array(
			'#type' => 'select',
			'#title' => t("Languages"),
			'#options' => $languages,
			'#default_value' => $default_fluencies_array,
			'#description' => t('Ctrl-click or Cmd-click to select more than one.'),
			'#size' => 10,
			'#multiple' => TRUE,
			'#chosen' => TRUE, // relies on chosen module: https://www.drupal.org/project/chosen
		);

		$form['staff_data']['certifications'] = array(
			'#type' => 'select',
			'#title' => t("Trainings & Certifications"),
			'#options' => $trainings,
			'#default_value' => $default_certifications_array,
			'#description' => t('Ctrl-click or Cmd-click to select more than one.'),
			'#size' => 4,
			'#multiple' => TRUE,
			'#chosen' => TRUE, // relies on chosen module: https://www.drupal.org/project/chosen
		);

		$form['staff_data']['orcid'] = array(
			'#type' => 'textfield',
			'#default_value' => $person->orcid,
			'#title' => t('ORCID'),
			'#description' => t('More information on <a href="https://scholarworks.duke.edu/orcid/">ORCID</a>'),
		);

		$form['staff_data']['libguide_id'] = array(
			'#type' => 'textfield',
			'#default_value' => $person->libguide_id,
			'#title' => t('Libguides Account ID'),
			'#description' => t('You can find this in the link that lists all of your libguides:<br />https://guides.library.duke.edu/prf.php?account_id=12345<br />(in this example the ID is 12345)'),
		);

		$form['staff_data']['libcal_id'] = array(
			'#type' => 'textfield',
			'#default_value' => $person->libcal_id,
			'#title' => t('Libcal Account ID'),
			'#description' => t('Add your ID here to enable appointments via libcal'),
		);

		
		$form_state['staff_data']['photo_old'] = $person->photo_id;
		$form_state['staff_data']['preferred_title_old'] = $person->preferred_title;
		$form_state['staff_data']['profile_old'] = $person->profile;
		$form_state['staff_data']['pronouns_old'] = $person->pronouns;
		$form_state['staff_data']['orcid_old'] = $person->orcid;
		$form_state['staff_data']['libguide_id_old'] = $person->libguide_id;
		$form_state['staff_data']['libcal_id_old'] = $person->libcal_id;

		$form_state['staff_data']['person_id'] = $person->id;
		$form_state['staff_data']['net_id'] = $person->net_id;

		$form_state['staff_data']['expertise_old'] = $default_expertise_array;
		$form_state['staff_data']['fluencies_old'] = $default_fluencies_array;
		$form_state['staff_data']['certifications_old'] = $default_certifications_array;
		
		$form['#submit'][] = 'dul_staff_profile_submit';

		# ===========================================================
		# Read-only fields from Duke HR: display as a collapsed group
		# ===========================================================
		$form['duke_hr_data'] = array(
			'#type' => 'fieldset',
			'#title' => 'Directory Info from Duke HR: ' . $person->display_name,
			'#description' => 'This info comes from SAP so it can\'t be edited here. Some fields (as marked) can be self-managed via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work</a> or <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service</a>.',
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#weight' => 4,
		);
		$form['duke_hr_data']['first_name'] = array(
			'#type' => 'item',
			'#title' => 'First Name',
			'#markup' => $person->first_name,
		);
		$form['duke_hr_data']['middle_name'] = array(
			'#type' => 'item',
			'#title' => 'Middle Name',
			'#markup' => $person->middle_name,
		);
		$form['duke_hr_data']['last_name'] = array(
			'#type' => 'item',
			'#title' => 'Last Name',
			'#markup' => $person->last_name,
		);
		$form['duke_hr_data']['nickname'] = array(
			'#type' => 'item',
			'#title' => 'Nickname',
			'#markup' => $person->nickname,
		);
		$form['duke_hr_data']['display_name'] = array(
			'#type' => 'item',
			'#title' => 'Display Name',
			'#markup' => $person->display_name,
			'#description' => 'Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service</a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>Change your name</strong></li><li>Click <strong>Update Display Name</strong> button</li>',
		);
		$form['duke_hr_data']['net_id'] = array(
			'#type' => 'item',
			'#title' => 'NetID',
			'#markup' => $person->net_id,
		);
		$form['duke_hr_data']['unique_id'] = array(
			'#type' => 'item',
			'#title' => 'Duke Unique ID',
			'#markup' => $person->unique_id,
		);
		$form['duke_hr_data']['title'] = array(
			'#type' => 'item',
			'#title' => 'Job Title',
			'#markup' => $person->title,
		);
		$form['duke_hr_data']['sap_org_unit'] = array(
			'#type' => 'item',
			'#title' => 'SAP Org Unit',
			'#markup' => $person->sap_org_unit,
		);
		$form['duke_hr_data']['primary_affiliation'] = array(
			'#type' => 'item',
			'#title' => 'Primary Affiliation',
			'#markup' => $person->primary_affiliation,
		);
		$form['duke_hr_data']['affiliation'] = array(
			'#type' => 'item',
			'#title' => 'Affiliation',
			'#markup' => $person->affiliation,
		);
		$form['duke_hr_data']['campus_box'] = array(
			'#type' => 'item',
			'#title' => 'Campus Box &mdash; labeled <em>"Mailing Address"</em> in DUL directory',
			'#markup' => $person->campus_box,
			'#description' => 'Edit via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work MyInfo</a> page; once there follow these steps:<ol><li>Click <strong>My Profile</strong></li><li>Click <strong>Maintain your Addresses & Phone Numbers</strong></li><li>Edit via <strong>Duke Interoffice Address</strong> field</ol>'
		);
		$form['duke_hr_data']['physical_address'] = array(
			'#type' => 'item',
			'#title' => 'Physical Address &mdash; labeled <em>"Location"</em> in DUL directory',
			'#markup' => $person->physical_address,
			'#description' => 'Edit via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work MyInfo</a> page; once there follow these steps:<ol><li>Click <strong>My Profile</strong></li><li>Click <strong>Maintain your Addresses & Phone Numbers</strong></li><li>Edit via <strong>Duke External Mailing Address</strong> field</ol>'
		);
		$form['duke_hr_data']['email'] = array(
			'#type' => 'item',
			'#title' => 'Preferred Email',
			'#markup' => $person->email,
			'#description' => 'Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service</a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>Change your preferred email address</strong></li>',
		);
		$form['duke_hr_data']['email_privacy'] = array(
			'#type' => 'item',
			'#title' => 'Email Privacy',
			'#markup' => $person->email_privacy,
			'#description' => 'Edit via <a href="https://idms-web.oit.duke.edu/portal/">Duke OIT Account Self-Service</a>: <ol><li>Click <strong>Manage Directory Listings</strong></li><li>Click <strong>Set who can see your email address</strong></li>'
		);
		$form['duke_hr_data']['phone'] = array(
			'#type' => 'item',
			'#title' => 'Phone',
			'#markup' => $person->phone,
			'#description' => 'Edit via <a href="https://work.duke.edu/irj/portal/MyInfo">Duke@Work MyInfo</a> page; once there follow these steps:<ol><li>Click <strong>My Profile</strong></li><li>Click <strong>Maintain your Addresses & Phone Numbers</strong></li><li>Under <strong>Duke External Mailing Address</strong>, click <strong>Edit</strong><li>Edit the <strong>Telephone</strong> field</li></ol>'
		);
		$form['duke_hr_data']['ldap_dn'] = array(
			'#type' => 'item',
			'#title' => 'LDAP DN',
			'#markup' => $person->ldap_dn,
		);
	}

	return $form;
}

/**
 * Callback function for dul_staff_form_user_profile_form_alter
 */
function dul_staff_profile_submit($form, &$form_state) {
	
	// ++ debug!! ++
	// drupal_set_message(kpr($form_state, TRUE));

	// what we plan to update
	$merge_fields = array();


	// DEAL WITH IMAGES

	// there is a new image to save
	if ($form_state['values']['photo'] != $form_state['staff_data']['photo_old'] && $form_state['values']['photo'] != NULL) {

		// first clean up old image
		if ($form_state['staff_data']['photo_old'] != NULL) {
			// fid of old uploaded image
			$delete_me_id = $form_state['staff_data']['photo_old'];


// --> This doesn't seem to work :(
			// remove file usage
			$delete_me = file_load($delete_me_id);
			file_usage_delete($delete_me, 'dul_system', 'user', 1);

			// unlink (delete) the image
			$uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :id", array(':id' => $delete_me_id))->fetchField();
			drupal_unlink($uri);
			
			// delete db entry
			$delete_image_record = db_delete('file_managed') 
			->condition('fid', $delete_me_id)
			->execute();

		}

		// set permanenet status
		if (!empty($form_state['values']['photo'])) {
				$file = file_load($form_state['values']['photo']);
				$file->status = FILE_STATUS_PERMANENT;
				$new_file_name = $file->filename;
				file_save($file);
				file_usage_add($file, 'dul_system', 'user', 1);


				// notify HR admin of upload:
				$email_recipient = $GLOBALS['email_notification_address'];
				$email_message = 'A user has changed their profile image as follows:';
				$email_message .= "\r\n\r\n";
				$email_message .= $form['duke_hr_data']['display_name']['#markup'];
				$email_message .= "\r\n";
				$email_message .= file_create_url('public://') . 'staff_images/' . $new_file_name;

				mail($email_recipient, 'New Staff Photo', $email_message);
		};

		// set value of photo_id as fid of new image
		$merge_fields['photo_id'] = $form_state['values']['photo'];

		// set value of photo_url as full path to new image
		$merge_fields['photo_url'] = file_create_url('public://') . 'staff_images/' . $new_file_name;

	} elseif ($form_state['staff_data']['photo_old'] != NULL && $form_state['values']['photo'] == NULL) {
	// there is an OLD image but NOT a new image to save -- clean up a removed image 

		// fid of old uploaded image
		$delete_me_id = $form_state['staff_data']['photo_old'];

		// get the uri of the old image
		$uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :id", array(':id' => $delete_me_id))->fetchField();

		// unlink (delete) the image
		drupal_unlink($uri);

		// delete db entry
		$delete_image_record = db_delete('file_managed') 
		->condition('fid', $delete_me_id)
		->execute();

		// reset photo ID/URL for update
		$merge_fields['photo_id'] = NULL;
		$merge_fields['photo_url'] = NULL;
	}


	if ($form_state['values']['preferred_title'] != $form_state['staff_data']['preferred_title_old']) {
		$merge_fields['preferred_title'] = $form_state['values']['preferred_title'];

		// notify HR admin of title update:
		$email_recipient = $GLOBALS['email_notification_address'];
		$email_message = 'A user has changed their preferred title as follows:';
		$email_message .= "\r\n\r\n";
		$email_message .= $form['duke_hr_data']['display_name']['#markup'];
		$email_message .= "\r\n";
		$email_message .= $merge_fields['preferred_title'];

		mail($email_recipient, 'New Staff Title', $email_message);

	}
	
	if ($form_state['values']['profile']['value'] != $form_state['staff_data']['profile_old']) {
		$merge_fields['profile'] = $form_state['values']['profile']['value'];
	}

	if ($form_state['values']['pronouns'] != $form_state['staff_data']['pronouns_old']) {
		$merge_fields['pronouns'] = $form_state['values']['pronouns'];
	}

	if ($form_state['values']['orcid'] != $form_state['staff_data']['orcid_old']) {
		$merge_fields['orcid'] = $form_state['values']['orcid'];
	}

	if ($form_state['values']['libguide_id'] != $form_state['staff_data']['libguide_id_old']) {
		$merge_fields['libguide_id'] = $form_state['values']['libguide_id'];
	}

	if ($form_state['values']['libcal_id'] != $form_state['staff_data']['libcal_id_old']) {
		$merge_fields['libcal_id'] = $form_state['values']['libcal_id'];
	}

	// We need to save data when $merge_fields has elements
	if (count($merge_fields)) {
		
		db_set_active('dul_directory');
	
		// As "profile" is accounted for (above ^), I added 'updated_at' to the 
		// $merge_fields and reduced the database calls to 1.
		// -dlc32
		$merge_fields['updated_at'] = date('Y-m-d H:i:s');
		$num_updated = db_merge('people')
			->key(array('id' => $form_state['staff_data']['person_id']))
			->fields($merge_fields)
			->execute();
			
		#$num_updated = db_update('people')
		#	->fields(array(
		#		'profile' => $form_state['values']['profile']['value'],
		#		'updated_at' => date('Y-m-d H:i:s'),
		#	))
		#	->condition('id', $form_state['staff_data']['person_id'])
		#	->execute();

		drupal_set_message(t('Your profile information has been saved'));

		// Restore the "Drupal" database as the active database
		db_set_active();
		
	}

	// update for expertise table
	db_set_active('dul_directory');

		// delete old records -- compare old to new
		$compare_old_expertise = array_diff($form_state['staff_data']['expertise_old'], $form_state['values']['expertise']);
		
		if (!empty($compare_old_expertise)) {
			foreach ($compare_old_expertise as $expertise_to_delete) {
				$expertise_deleted = db_delete('expertise')
				->condition('person_id', $form_state['staff_data']['person_id'])
				->condition('subject_area_id', $expertise_to_delete)
				->execute();
			}
		};

		// add new records -- compare new to old
		$compare_new_expertise = array_diff($form_state['values']['expertise'], $form_state['staff_data']['expertise_old']);

		if (!empty($compare_new_expertise)) {
			foreach ($compare_new_expertise as $expertise_to_add) {
				$expertise_added = db_insert('expertise')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'subject_area_id' => $expertise_to_add,
					'created_at' => date('Y-m-d H:i:s'),
			 		'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		} else {
			foreach ($form_state['values']['expertise'] as $expertise_to_add) {
				$expertise_added = db_insert('expertise')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'subject_area_id' => $expertise_to_add,
					'created_at' => date('Y-m-d H:i:s'),
			 		'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		};

	db_set_active();



	// update for fluencies table
	db_set_active('dul_directory');

		// delete old records -- compare old to new
		$compare_old_fluencies = array_diff($form_state['staff_data']['fluencies_old'], $form_state['values']['fluencies']);
		
		if (!empty($compare_old_fluencies)) {
			foreach ($compare_old_fluencies as $fluencies_to_delete) {
				$fluencies_deleted = db_delete('fluencies')
				->condition('person_id', $form_state['staff_data']['person_id'])
				->condition('language_id', $fluencies_to_delete)
				->execute();
			}
		};

		// add new records -- compare new to old
		$compare_new_fluencies = array_diff($form_state['values']['fluencies'], $form_state['staff_data']['fluencies_old']);

		if (!empty($compare_new_fluencies)) {
			foreach ($compare_new_fluencies as $fluencies_to_add) {
				$fluencies_added = db_insert('fluencies')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'language_id' => $fluencies_to_add,
					'created_at' => date('Y-m-d H:i:s'),
			 		'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		} else {
			foreach ($form_state['values']['fluencies'] as $fluencies_to_add) {
				$fluencies_added = db_insert('fluencies')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'language_id' => $fluencies_to_add,
					'created_at' => date('Y-m-d H:i:s'),
			 		'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		};

	db_set_active();

	// update for certifications table
	db_set_active('dul_directory');

		// delete old records -- compare old to new
		$compare_old_certifications = array_diff($form_state['staff_data']['certifications_old'], $form_state['values']['certifications']);

		if (!empty($compare_old_certifications)) {
			foreach ($compare_old_certifications as $certifications_to_delete) {
				$certifications_deleted = db_delete('certifications')
				->condition('person_id', $form_state['staff_data']['person_id'])
				->condition('training_id', $certifications_to_delete)
				->execute();
			}
		};

		// add new records -- compare new to old
		$compare_new_certifications = array_diff($form_state['values']['certifications'], $form_state['staff_data']['certifications_old']);

		if (!empty($compare_new_certifications)) {
			foreach ($compare_new_certifications as $certifications_to_add) {
				$certifications_added = db_insert('certifications')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'training_id' => $certifications_to_add,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		} else {
			foreach ($form_state['values']['certifications'] as $certifications_to_add) {
				$certifications_added = db_insert('certifications')
				->fields(array(
					'person_id' => $form_state['staff_data']['person_id'],
					'training_id' => $certifications_to_add,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				))
				->execute();
			}
		};

	db_set_active();

}


// managed file elements process callback

function custom_managed_file_element_process($element, &$form_state, $form) {

  $element = file_managed_file_process($element, $form_state, $form);
  
  $element['#pre_render'][] = 'custom_managed_file_element_preview';
  
  return $element;
}

// elements pre render callback

function custom_managed_file_element_preview(array $element) {

  if (empty($element['#file'])) {
    
		hide($element['remove_button']);
		
		// show default avatar image
    
		// $path_to_default_avatar = '/' . drupal_get_path('module', 'dul_system') . '/images/default_avatar.png';
		// note -- can't figure out how to have default preview thumbnail live outside the public folder
		
		$path_to_default_avatar = 'public://staff_images/default_avatar.png';

		$variables = array (
			'alt' => 'reading devil',
			'path' => $path_to_default_avatar,
			'style_name' => 'thumbnail',
			'attributes' => array (
				'class' => array ('upload-preview')
			),
		);
		
		$element['preview'] = array (
			'#type' => 'markup',
			'#weight' => -150,
			'#markup' => theme('image_style', $variables),
		);

  }
  else {

    $file = $element['#file'];
    hide($element['upload']);
    hide($element['upload_button']);

    if (!file_validate_is_image($file)) {
      $info = image_get_info($file->uri);
      $variables = array (
        'alt' => 'staff thumbnail',
        'path' => $file->uri,
        'style_name' => 'thumbnail',
        'attributes' => array (
          'class' => array ('upload-preview'),
				),
			);

      if (is_array($info)) {
        $variables += $info;
      }

      $element['preview'] = array (
        '#type' => 'markup',
        '#weight' => -150,
        '#markup' => theme('image_style', $variables),
			);
    }
  }

  return $element;

}
