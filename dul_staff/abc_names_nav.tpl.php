<?php

/**
 * @file
 * Partial reusable template to render navigation by letter for staff browse
 */
?>

<nav class="abc-nav" aria-label="Browse staff by letter" role="navigation">
  <?php $all_letters = range('A', 'Z'); ?>
  <div class="btn-group" role="group">
      <a class="btn btn-default<?php if(isset($starting_letter) and $starting_letter == 'all'): ?> active<?php endif; ?>" href="<?php print $GLOBALS['browse_path']; ?>all">ALL</a>
    <?php foreach($all_letters as $letter): ?>
      <a class="btn btn-default<?php if(!in_array($letter, $abc_nav)): ?> disabled<?php endif; ?><?php if(isset($starting_letter) and $starting_letter == strtolower($letter)): ?> active<?php endif; ?>" href="<?php print $GLOBALS['browse_path'] . strtolower($letter); ?>"><?php print $letter ?></a>
    <?php endforeach; ?>
  </div>
</nav>

