/**
 * @file
 */
(function($) {

  $(document).ready(function(){

    $(".staff-directory-nav .browse-buttons a").each(function(){
      if($(this).attr('href') == window.location.pathname) {
        $(this).addClass('active');
      }
    });


    /* ++ Expand / Contract ++ */

    // test if an element is in the viewport
    $.fn.isInViewport = function() {
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();
      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $(".expandable-content-wrapper .less").click(function(evt) {
      evt.preventDefault();
      var theAnchorID = $(this).find(".btn").attr('href');
      if (!$(theAnchorID).isInViewport()) {
        $('html,body').animate({scrollTop: $(theAnchorID).offset().top - 50}, 'fast');
      }
    });

    var lineHeight = function(element) {
      var pxHeight = window.getComputedStyle(element, null).getPropertyValue("line-height");
      return parseInt(pxHeight.replace(/px$/, ''), 10);
    }


    $(".expandable-content").each( function() {

      var me = $(this);
      var parent = me.parent();
      var origHeight = me.height();
      var contractedHeight = lineHeight(this) * 10.25;

      if ( origHeight <= contractedHeight ) {
        return;
      }

      var contract = function(evt) {
        me.addClass('contracted');
        // if ( me.children(":first").is("ul") ) {
        //   me.addClass('contracted-ul');
        // }
        parent.attr('aria-expanded', 'false');
      }

      var expand = function(evt) {
        me.removeClass('contracted');
        // me.removeClass('contracted-ul');
        parent.attr('aria-expanded', 'true');
      };

      contract(); // if we got this far, we want to shrink

      parent.find('.more').click(function() {
        expand();
      });

      parent.find('.less').click(function() {
        contract();
      });

    });

  });

})(jQuery);