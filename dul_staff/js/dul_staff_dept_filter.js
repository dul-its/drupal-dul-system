/**
 * @file
 */
(function($) {

  $(document).ready(function(){

    // Prevent form submit for dept search; use live-filter instead
    $("form#dept-filter").submit(function(e) {
      e.preventDefault();
    });

    // Live-filter the subject specialists page
    $("input#dept-search").on("keyup", function() {
      $("#dept-filter-results").show();
      $("#directory-main-content").hide();
      
      var value = $(this).val().toLowerCase();
      $("#dept-filter-results li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });

      if (value.length === 0) { // if removed the search string...
        $("#dept-filter-results").hide();
        $("#directory-main-content").show();
      }
    });

  });

})(jQuery);
