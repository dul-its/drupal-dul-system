<?php

/**
 * @file
 * Default theme implementation to display a page listing staff contacts by subject area
 *
 * Available variables:
 * - $subject_tree: array of nested stdClass Objects; all supported subjects
 */
?>
<!-- uncomment print to inspect records in browser -->
<?php #print kpr($subject_tree, TRUE, "Subject Tree"); ?>

<?php
  drupal_add_js('sites/all/modules/dul_system/dul_staff/js/dul_staff_subject_filter.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php 
  drupal_add_js('/sites/all/modules/dul_system/dul_staff/js/dul_staff.js', array(
    'type' => 'file',
    'group' => JS_THEME,
  ));
?>

<?php
  // get shared nav
  include_once 'dul_staff.header_nav.inc';
?>

<div id="directory-main-content" class="container subject-specialists">


  <div class="row">

    <div class="col-sm-12">
      <form id="subject-specialist-filter" class="directory-filter">
        <label for="subject-search">Filter by Subject or Name</label>
        <div class="search-wrapper">
          <input type="text" id="subject-search" name="subject-search"/>
        </div>
      </form>
    </div>

    <div id="subject-specialist-list" class="col-sm-12">
      <?php foreach($subject_tree as $subject): ?>
        <div class="subject-group">
          <h2 class="subject-title" id="<?php print $subject->id ?>"><?php print $subject->title ?></h2>

          <?php if(isset($subject->staff_specialists)): ?>
            <?php foreach($subject->staff_specialists as $member): ?>

            <?php // fallback to ID if slug is not present
              if ($member->slug) {
                $slugLink = $member->slug;
              } else {
                $slugLink = $member->id;
              }
            ?>

              <div class="row subject-specialist-row">
                <div class="col-md-1 col-sm-3 col-xs-4">
                  <a href="<?php print $GLOBALS['staff_profile_path'] . $slugLink; ?>">
                    <?php dul_staff_person_photo($member, 'staff_dir_thumb'); ?>
                  </a>
                </div>
                <div class="col-md-11 col-sm-9 col-xs-12">
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="person-name">
                        <a href="<?php print $GLOBALS['staff_profile_path'] . $slugLink; ?>">
                          <?php print $member->display_name; ?>
                        </a>
                      </div>
                      <div class="person-job-title"><?php print $member->preferred_title ? $member->preferred_title : $member->title; ?></div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <ul class="contact-info list-unstyled">
                        <?php if (!empty($member->email)): //and !($member->email_privacy) ?>
                          <li class="contact-email"><a href="mailto:<?php print $member->email ?>"><?php print $member->email ?></a></li>
                        <?php endif ?>
                        <?php if (!empty($member->phone)): ?>
                          <li class="contact-phone">
                            <?php print $member->phone ?>
                          </li>
                        <?php endif ?>
                      </ul>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                      <ul class="list-unstyled subject-specialist-actions">
                        <?php if (!empty($member->libguide_id)): ?>
                          <li><a class="btn btn-default btn-sm" href="https://guides.library.duke.edu/prf.php?account_id=<?php print $member->libguide_id; ?>">View Research Guides</a></li>
                        <?php endif ?>
                        <?php if (!empty($member->libcal_id)): ?>
                          <li><a class="btn btn-default btn-sm" href="https://duke.libcal.com/appointment/<?php print $member->libcal_id; ?>">Schedule an Appointment</a></li>
                        <?php endif ?>
                      </ul>
                    </div>
                  </div>
                </div>

              </div> <!-- /subject-specialist-row -->
            <?php endforeach; ?>
          <?php endif; ?>


          <?php if(isset($subject->external_specialists)): ?>
            <?php foreach($subject->external_specialists as $contact_point): ?>

              <div class="row subject-specialist-row">
                <div class="col-md-1 col-sm-3 col-xs-4">
                  <a href="<?php print $contact_point->url; ?>" aria-label="<?php print $contact_point->name; ?>">
                    <img class="person-thm"
                      src="<?php print $GLOBALS['devil_missing_photo_path']; ?>"
                      alt=""/>
                  </a>
                </div>
                <div class="col-md-11 col-sm-9 col-xs-12">
                  <div class="row">
                    <div class="col-md-5 col-sm-12">
                      <div class="person-name">
                        <a href="<?php print $contact_point->url; ?>">
                          <?php print $contact_point->name; ?>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                      <?php print $contact_point->description; ?>
                    </div>
                  </div>
                </div>

              </div> <!-- /subject-specialist-row -->

            <?php endforeach; ?>
          <?php endif; ?>
        </div> <!-- /subject-group -->
      <?php endforeach; ?>
    </div>
  </div>
</div>
